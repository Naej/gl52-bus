package sample.view;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class LineBendDisplay extends SelectableBusNetworkElement {


    private float ray = 7;
    private Color lineColor;

    public LineBendDisplay(Point2D pos, Color lineColor){
        this.position = pos;
        this.lineColor = lineColor;

    }

    public void display(MovableJfxCamera camera) {


        if (selected) {
            Point2D displayedPos = camera.fromWorldToCameraSpace(getPosition().subtract(new Point2D(ray,ray)));
            camera.getGraphicsContext().setStroke(Color.BLACK);
            camera.getGraphicsContext().setLineWidth(1);
            camera.getGraphicsContext().strokeOval(displayedPos.getX(),displayedPos.getY(),ray*2,ray*2);

        }
    }

    public void onMouseEvent(MouseEvent event, MovableJfxCamera camera) {
        if (isMouseHovering(event,camera) && "MOUSE_CLICKED".equals(event.getEventType().getName())) {
            for (SelectableEventListener listener : listeners){
                listener.onSelectableClick(this, event);
            }
        }
    }

    private boolean isMouseHovering(MouseEvent event,MovableJfxCamera camera) {
        return getMouseWorldPosition(event,camera).distance(getPosition()) < ray;
    }

    public Color getLineColor() {
        return lineColor;
    }
}
