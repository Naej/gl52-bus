package sample.view;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import sample.model.BusStop;


public class BusStopDisplay extends SelectableBusNetworkElement implements CoverageUpdateListener{

    private float ray = 5;
    private Color lineColor;

    private boolean showcoverage = false;
    private double coveragesize = 50;

    public String name = "";
    public String horaires1 = "";
    public String horaires2 = "";

    public BusStopDisplay(Point2D pos, Color color){
        this.position = pos;
        this.lineColor = color;

    }

    public void display(MovableJfxCamera camera) {
        Point2D displayedPos = camera.fromWorldToCameraSpace(getPosition().subtract(new Point2D(ray,ray)));

        camera.getGraphicsContext().setStroke(lineColor);
        camera.getGraphicsContext().setLineWidth(3);
        if (selected) {
            camera.getGraphicsContext().setStroke(lineColor.deriveColor(1,1,1,0.5));
        }
        camera.getGraphicsContext().strokeOval(displayedPos.getX(),displayedPos.getY(),ray*2,ray*2);


        if (showcoverage) {
            drawCoverage(camera);
        }

        camera.getGraphicsContext().setStroke(lineColor);


    }

    public void onMouseEvent(MouseEvent event, MovableJfxCamera camera) {
        if (isMouseHovering(event,camera) && "MOUSE_CLICKED".equals(event.getEventType().getName()) ) {
            for (SelectableEventListener listener : listeners){
                listener.onSelectableClick(this, event);
            }
        }
    }

    private boolean isMouseHovering(MouseEvent event,MovableJfxCamera camera) {
        return getMouseWorldPosition(event,camera).distance(getPosition()) <= ray;
    }

    public Color getColor(){
        return lineColor;
    }

    public void drawCoverage(MovableJfxCamera camera) {

        Point2D displayedPos = camera.fromWorldToCameraSpace(getPosition().subtract(new Point2D(coveragesize/2,coveragesize/2)));

        camera.getGraphicsContext().setLineWidth(3);

        camera.getGraphicsContext().setFill(lineColor.deriveColor(1,1,1,0.5));


        camera.getGraphicsContext().fillOval(displayedPos.getX(),displayedPos.getY(),coveragesize,coveragesize);
    }

    public void onCoverageUpdate(boolean showCoverage, double coverageScale) {
        this.coveragesize = coverageScale;
        this.showcoverage = showCoverage;
    }

    public BusStop generateBusStop() {
        return new BusStop(name,getPosition());
    }
}
