package sample.view;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;

public interface Interactable {
    void onMouseEvent(MouseEvent event, MovableJfxCamera camera);
}
