package sample.view;


import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;

public interface SelectableEventListener {
    void onSelectableClick(Selectable selectable, MouseEvent event);
    void onSelectableMove(Selectable selectable, Point2D previousPosition);
}
