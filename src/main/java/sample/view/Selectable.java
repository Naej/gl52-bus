package sample.view;

public interface Selectable {
    void registerListener(SelectableEventListener listener);
}
