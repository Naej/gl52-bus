package sample.view;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import sample.model.BusStop;
import sample.model.SegmentOfWay;
import sample.model.Way;

import java.util.ArrayList;

public class LineDisplay extends BusNetworkElement {

    private ArrayList<BusNetworkElement> joints = new ArrayList<BusNetworkElement>();
    private Color lineColor;

    public LineDisplay(Color color){
        lineColor = color;
    }

    public void display(MovableJfxCamera camera) {
        BusNetworkElement prev = null;
        for (BusNetworkElement joint  : joints ) {
            joint.display(camera);

            if(prev != null) {
                drawLine(joint.getPosition(),prev.getPosition(),camera);
            }
            prev = joint;
        }
    }

    public void onMouseEvent(MouseEvent event, MovableJfxCamera camera) {
        for(BusNetworkElement joint : joints){
            joint.onMouseEvent(event,camera);
        }

    }

    private void drawLine(Point2D a,Point2D b, MovableJfxCamera camera) {
        Point2D ac = camera.fromWorldToCameraSpace(a);
        Point2D bc = camera.fromWorldToCameraSpace(b);
        camera.getGraphicsContext().setStroke(lineColor);
        camera.getGraphicsContext().setLineWidth(3);
        camera.getGraphicsContext().strokeLine(ac.getX(),ac.getY(),bc.getX(),bc.getY());
    }

    public void addJoint(BusNetworkElement joint){
        if (!joints.isEmpty() || joint instanceof BusStopDisplay) {
            joints.add(joint);
        }
    }

    public void removeJoint(BusNetworkElement joint) {
        joints.remove(joint);
    }

    public ArrayList<Way> generateWays(){
        ArrayList<Way> ways = new ArrayList<Way>();

        ArrayList<SegmentOfWay> currentSegments = new ArrayList<SegmentOfWay>();
        BusNetworkElement previous = null;
        BusStop previousBusStop = null;

        for (BusNetworkElement joint : joints) {
            if (previous != null && previousBusStop != null) {
                currentSegments.add(new SegmentOfWay(previous.getPosition(),joint.getPosition()));
                if (joint instanceof BusStopDisplay) {
                    ways.add(new Way(currentSegments,previousBusStop,((BusStopDisplay) joint).generateBusStop()));
                    currentSegments = new ArrayList<SegmentOfWay>();
                }
            }
            previous = joint;
            if (joint instanceof BusStopDisplay) {
                previousBusStop = ((BusStopDisplay) joint).generateBusStop();
            }
        }
        if (!currentSegments.isEmpty()) {
            System.out.println("Line not finished, dropped the end");
        }
        return ways;
    }
}
