package sample.view;

public interface CoverageUpdateListener {
    void onCoverageUpdate(boolean showCoverage, double coverageScale);
}
