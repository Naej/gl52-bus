package sample.view;

import camera.MovableJfxCamera;
import displayable.Displayable;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;


public abstract class BusNetworkElement implements Displayable<MovableJfxCamera>, Interactable{

    protected Point2D position;

    public Point2D getPosition() {
        return position;
    }


    protected Point2D getMouseWorldPosition(MouseEvent e, MovableJfxCamera camera) {
        return camera.fromScreenToWorldSpace(new Point2D(e.getX(), e.getY()));
    }


}
