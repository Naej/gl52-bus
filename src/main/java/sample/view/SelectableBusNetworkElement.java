package sample.view;

import javafx.geometry.Point2D;

import java.util.ArrayList;

public abstract class SelectableBusNetworkElement extends BusNetworkElement implements Selectable{
    public boolean selected = false;
    protected ArrayList<SelectableEventListener> listeners = new ArrayList<SelectableEventListener>();

    public void registerListener(SelectableEventListener listener) {
        listeners.add(listener);
    }

    public void setPosition(Point2D pos) {
        for (SelectableEventListener listener : listeners) {
            listener.onSelectableMove(this,getPosition());
        }
        position = pos;
    }
}
