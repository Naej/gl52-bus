package sample.model;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ssessou on May, 2019
 */
public class SegmentOfWay {

    private Point2D start;
    private Point2D end;
    private List<Bus> currentBus;

    public SegmentOfWay(Point2D start, Point2D end) {
        this.start = start;
        this.end = end;
        this.currentBus = new ArrayList<Bus>();
    }

    public Point2D getStart() {
        return start;
    }

    public Point2D getEnd() {
        return end;
    }

    public List<Bus> getCurrentBus() {
        return currentBus;
    }

    public void setStart(Point2D start) {
        this.start = start;
    }

    public void setEnd(Point2D end) {
        this.end = end;
    }

    public void setCurrentBus(List<Bus> currentBus) {
        this.currentBus = currentBus;
    }
}
