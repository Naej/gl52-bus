package sample.model;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssessou on May, 2019
 */
public class Line {

    private ArrayList<Way> ways;
    private Color color;

    public Line(Color color) {
        this.ways = new ArrayList<Way>();
        this.color = color;
    }

    public List<Way> getWays() {
        return ways;
    }

    public Color getColor() {
        return color;
    }

    public void setWays(ArrayList<Way> ways) {
        this.ways = ways;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void addWay(Way way) {
        ways.add(way);
    }
}
