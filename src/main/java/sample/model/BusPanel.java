package sample.model;

import javafx.geometry.Point2D;

import java.util.Date;
import java.util.List;

/**
 * Created by ssessou on May, 2019
 */
public class BusPanel {

    private List<Date> passages;

    public BusPanel(Point2D position, List<Date> passages, BusStop stop, Way way) {
        this.passages = passages;
    }

    public List<Date> getPassages() {
        return passages;
    }

    public void setPassages(List<Date> passages) {
        this.passages = passages;
    }

}
