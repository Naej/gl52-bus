package sample.model;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ssessou on May, 2019
 */
public class BusNetwork {

    private HashMap<Color,Line> lines = new HashMap<Color, Line>();
    private ArrayList<Bus> bus = new ArrayList<Bus>();

    public Collection<Line> getLines() {
        return lines.values();
    }

    public List<Bus> getBus() {
        return bus;
    }

    public void createLine(Color lineColor) {
        if (!lines.containsKey(lineColor)) {
            lines.put(lineColor,new Line(lineColor));
        }
    }

    public Line getLine(Color lineColor) {
        return lines.get(lineColor);
    }


}
