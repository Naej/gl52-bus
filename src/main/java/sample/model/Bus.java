package sample.model;

import java.util.Iterator;

/**
 * Created by ssessou on May, 2019
 */
public class Bus {

    private int id;
    private boolean arrived = false;
    private SegmentOfWay segmentOfWay;
    private Iterator<SegmentOfWay> wayIterator;
    private float capacity = 80;
    private float speed = 50;

    public Bus(int id, Way way) {
        this.id = id;
        this.wayIterator = way.loadIterator();
        if(wayIterator.hasNext()){
            this.segmentOfWay = wayIterator.next();
        }
        else{
            arrived = true;
        }
    }

    public int getId() {
        return id;
    }

    public boolean isArrived() {
        return arrived;
    }

    public SegmentOfWay getSegmentOfWay() {
        return segmentOfWay;
    }

    public Iterator<SegmentOfWay> getWayIterator() {
        return wayIterator;
    }

    public float getCapacity() {
        return capacity;
    }

    public float getSpeed() {
        return speed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setArrived(boolean arrived) {
        this.arrived = arrived;
    }

    public void setSegmentOfWay(SegmentOfWay segmentOfWay) {
        this.segmentOfWay = segmentOfWay;
    }

    public void setWayIterator(Iterator<SegmentOfWay> wayIterator) {
        this.wayIterator = wayIterator;
    }

    public void setCapacity(float capacity) {
        this.capacity = capacity;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
