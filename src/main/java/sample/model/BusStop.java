package sample.model;

import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssessou on May, 2019
 */
public class BusStop {

    private String name;
    private BusPanel panel;
    private Point2D position;

    public BusStop(String name, Point2D position) {
        this.name = name;
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public BusPanel getPanels() {
        return panel;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPanels(BusPanel panel) {
        this.panel = panel;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }
}
