package sample.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ssessou on May, 2019
 */
public class Way {

    private ArrayList<SegmentOfWay> segments;
    private BusStop start;
    private BusStop end;

    public Way(ArrayList<SegmentOfWay> segments, BusStop start, BusStop end) {
        this.segments = segments;
        this.start = start;
        this.end = end;
    }

    public ArrayList<SegmentOfWay> getSegments() {
        return segments;
    }

    public BusStop getStart() {
        return start;
    }

    public BusStop getEnd() {
        return end;
    }

    public void setSegments(ArrayList<SegmentOfWay> segments) {
        this.segments = segments;
    }

    public void setStart(BusStop start) {
        this.start = start;
    }

    public void setEnd(BusStop end) {
        this.end = end;
    }

    public Iterator<SegmentOfWay> loadIterator(){
        return segments.iterator();
    }
}
