package sample.controller;

import camera.MovableJfxCamera;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import sample.model.BusNetwork;
import sample.view.*;

import java.util.ArrayList;
import java.util.HashMap;


public class BusNetworkController extends SelectableBusNetworkElement implements SelectableEventListener {

    private BusNetwork data;

    private boolean showCoverage = false;
    private double coverageSize = 50;

    private HashMap<Color, LineDisplay> Lines = new HashMap<Color, LineDisplay>();

    private ArrayList<CoverageUpdateListener> coverageListeners = new ArrayList<CoverageUpdateListener>();

    public BusNetworkController(){
        data = new BusNetwork();
    }

    public void display(MovableJfxCamera camera) {
        for (LineDisplay line : Lines.values()){
            line.display(camera);
        }
    }

    public void onMouseEvent(MouseEvent event,MovableJfxCamera camera) {
        for (LineDisplay line : Lines.values()){
            line.onMouseEvent(event,camera);
        }
    }

    public LineDisplay getLine(Color lineColor) {
        return Lines.get(lineColor);
    }

    public void createLine(Color lineColor) {
        data.createLine(lineColor);

        LineDisplay newLine = new LineDisplay(lineColor);
        Lines.put(lineColor, newLine);
    }

    public void addJoint(Color lineColor, SelectableBusNetworkElement joint){
        Lines.get(lineColor).addJoint(joint);
        joint.registerListener(this);

        if(joint instanceof CoverageUpdateListener) {
            coverageListeners.add((CoverageUpdateListener)joint);
            ((CoverageUpdateListener) joint).onCoverageUpdate(showCoverage,coverageSize);
        }

        data.getLine(lineColor).setWays(Lines.get(lineColor).generateWays());

    }

    public void onSelectableClick(Selectable stop, MouseEvent event) {
        for (SelectableEventListener listener: listeners){
            listener.onSelectableClick(stop,event);
        }
    }

    public void onSelectableMove(Selectable selectable, Point2D previousPosition) {
        Color lineColor = null;
        if (selectable instanceof BusStopDisplay) {
            lineColor = ((BusStopDisplay) selectable).getColor();
        }
        if (selectable instanceof LineBendDisplay) {
            lineColor = ((LineBendDisplay) selectable).getLineColor();
        }
        data.getLine(lineColor).setWays(Lines.get(lineColor).generateWays());
    }

    public void toggleCoverage(){
        showCoverage = !showCoverage;
        updateCoverage();
    }

    public void updateCoverageSize(double value){
        coverageSize = value;
        updateCoverage();
    }

    private void updateCoverage() {
        for (CoverageUpdateListener listener : coverageListeners) {
            listener.onCoverageUpdate(showCoverage,coverageSize);
        }
    }




}
