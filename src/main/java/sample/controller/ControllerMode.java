package sample.controller;

public enum ControllerMode {
    DRAW,
    EDIT,
    MOVE
}
