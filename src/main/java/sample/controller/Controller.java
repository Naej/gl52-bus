package sample.controller;

import camera.MovableJfxCamera;
import displayable.Displayable;
import displayable.JfxSprite;

import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import sample.view.*;

import java.util.ArrayList;

public class Controller implements SelectableEventListener {
    @FXML
    private Canvas myCanvas;

    @FXML
    private ColorPicker myColorPicker;

    @FXML
    private Slider zoomSlider;
    @FXML
    private Slider coverageSlider;

    @FXML
    private ToggleButton coverageToggle;

    @FXML
    private Button currentLineDisplay;

    @FXML
    private TextArea busTextField1;

    @FXML
    private TextArea busTextField2;

    @FXML
    private TextField currentStopName;

    private ControllerMode mode = ControllerMode.DRAW;

    private Color currentColor = Color.BLUE;

    private MovableJfxCamera camera;
    private ArrayList<Displayable> toDisplay = new ArrayList<Displayable>();

    private JfxSprite map;

    private BusNetworkController busNetwork = new BusNetworkController();

    private Selectable selected = null;

    @FXML
    private void initialize()
    {
        camera = new MovableJfxCamera(myCanvas);
        map = new JfxSprite(new Point2D(0,0),new Image("/map.png"));
        toDisplay.add(map);
        toDisplay.add(busNetwork);

        myColorPicker.setValue(currentColor);
        myColorPicker.getCustomColors().add(currentColor);

        colorChange();
        deselect();
        busNetwork.registerListener(this);
        updateView();
    }

    public void onMouseClick(MouseEvent e){
        switch (mode) {
            case DRAW:
                if(e.getButton() == MouseButton.PRIMARY) {
                    busNetwork.addJoint(currentColor,new BusStopDisplay(getMouseWorldPosition(e,camera),currentColor));
                } else {
                    busNetwork.addJoint(currentColor,new LineBendDisplay(getMouseWorldPosition(e,camera),currentColor));
                }
                break;
            case EDIT:
                if (e.getButton() == MouseButton.PRIMARY) {
                    busNetwork.onMouseEvent(e, camera);
                }
                break;
            case MOVE:
                camera.centerOn(getMouseWorldPosition(e,camera));
        }

        updateView();
    }



    public void onMouseMove(MouseEvent e){
        switch (mode) {
            case EDIT:
                if (e.isControlDown()) {
                    if(selected instanceof SelectableBusNetworkElement){
                        ((SelectableBusNetworkElement) selected).setPosition(getMouseWorldPosition(e,camera));
                    }
                }
                break;
        }
        updateView();
    }

    private Point2D getMouseWorldPosition(MouseEvent e, MovableJfxCamera camera) {
        return camera.fromScreenToWorldSpace(new Point2D(e.getX(), e.getY()));
    }

    public void drawMode(){
        mode = ControllerMode.DRAW;
        deselect();

    }

    public void editMode(){
        mode = ControllerMode.EDIT;

    }

    public void moveMode(){
        mode = ControllerMode.MOVE;
        deselect();
    }

    public void colorChange() {
        currentColor = myColorPicker.getValue();
        if (busNetwork.getLine(currentColor) == null) {
            busNetwork.createLine(currentColor);
        }
        currentLineDisplay.setTextFill(currentColor);
    }

    public void onSelectableClick(Selectable selectable, MouseEvent event) {
        if(selectable instanceof SelectableBusNetworkElement) {
            deselect();
            select(selectable);
        }
    }

    public void onSelectableMove(Selectable selectable, Point2D previousPosition) {
        // do nothing
    }

    public void select(Selectable selectable) {
        ((SelectableBusNetworkElement) selectable).selected = true;
        selected = selectable;
        if (selectable instanceof BusStopDisplay) {
            currentStopName.setEditable(true);
            currentStopName.setText(((BusStopDisplay) selectable).name);
            busTextField1.setEditable(true);
            busTextField1.setText(((BusStopDisplay) selectable).horaires1);
            busTextField2.setEditable(true);
            busTextField2.setText(((BusStopDisplay) selectable).horaires2);
        }

    }

    private void deselect() {
        if (selected instanceof SelectableBusNetworkElement) {
            ((SelectableBusNetworkElement) selected).selected = false;
        }
        selected = null;
        currentStopName.setEditable(false);
        busTextField1.setEditable(false);
        busTextField2.setEditable(false);
    }

    public void onZoomUpdate() {
        updateView();
    }

    public void onCoverageUpdate() {
        busNetwork.updateCoverageSize(coverageSlider.getValue());
        updateView();
    }

    public void toggleCoverage() {
        busNetwork.toggleCoverage();
        updateView();
    }

    private void updateView() {
        camera.setZoom(zoomSlider.getValue());
        camera.display(toDisplay);
    }

    public void onBusInfoUpdate() {
        if (selected instanceof BusStopDisplay) {
            ((BusStopDisplay) selected).name = currentStopName.getText();
            ((BusStopDisplay) selected).horaires1 = busTextField1.getText();
            ((BusStopDisplay) selected).horaires2 = busTextField2.getText();
        }
    }
}
